import reactLogo from './assets/react.svg';
import './App.css';
import { Route, Routes } from 'react-router-dom';
import Layout from './layouts/Layout';
import { Home } from './pages/Home';
import { Register } from './pages/Register';
import { FormInformation } from './components/FormInformation';
import { FormTypeEmail } from './components/FormTypeEmail';
import { ForgetPassword } from './pages/ForgetPassword';
import OTP from './pages/ForgetPassword/OTP';
import { Cart } from './pages/Cart';
import ShopPage from './pages/Shop/ShopPage';
import { Contact } from './pages/Contact';
import { Order } from './pages/Order';
import { Details } from './pages/Details';
import { PageNotFound } from './pages/PageNotFound';
import { Login } from './pages/Login';

function App() {
    return (
        <Routes>
            <Route element={<Layout />}>
                <Route path="/" index element={<Home />} />
                <Route path="/register" element={<Register />}>
                    <Route index path="formInfor" element={<FormInformation />} />
                    <Route path="formEmail" element={<FormTypeEmail />} />
                </Route>
                <Route path="/forgetpass" element={<ForgetPassword />}>
                    <Route index path="typePass" element={<FormTypeEmail />} />
                    <Route path="otp" element={<OTP />} />
                </Route>
                <Route path="/pagecart" element={<Cart />} />
                <Route path="/shop" element={<ShopPage />} />
                <Route path="/contact" element={<Contact />} />
                <Route path="/order" element={<Order />} />
                <Route path="/detail/:id" element={<Details />} />
                {/* <Route path="/pageprofile" element={<PageProfile />} />
                <Route path="/pagechangepass" element={<PageChangePass />} />
                <Route path="/history-order" element={<HistoryOrder />}></Route> */}
                <Route path="/home" element={<Home />} />
                <Route path="/login" element={<Login />} />
            </Route>
            <Route path="*" element={<PageNotFound />} />
        </Routes>
    );
}

export default App;
