import imgHome1 from '/images/home/coming1.jpg';
import imgHome2 from '/images/home/coming2.jpg';
import imgHome3 from '/images/home/coming3.jpg';
import imgHome4 from '/images/home/coming4.jpg';
import imgHome5 from '/images/home/coming5.jpg';
import imgHome6 from '/images/home/coming6.jpg';
import imgHome7 from '/images/home/coming7.jpg';
import imgHome8 from '/images/home/coming8.jpg';

import imgSlide1 from '/images/slide/slide1.png';
import imgSlide2 from '/images/slide/slide2.png';
import imgSlide3 from '/images/slide/slide3.png';

import loadingGif from '/images/loading/loading.svg';

 const homeBanner =  {
    imgHome1,
    imgHome2,
    imgHome3,
    imgHome4,
    imgHome5,
    imgHome6,
    imgHome7,
    imgHome8,
}

const slideBanner = {
    imgSlide1,
    imgSlide2,
    imgSlide3,
}

const loadingApp = {
    loadingGif,
}


export {
    homeBanner,
    slideBanner,
    loadingApp,
}
 

