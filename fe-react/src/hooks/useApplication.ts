
import { AppContextProps, ApplicationContext } from "@/context";
import { useContext } from "react";

 export default function useApplication(){
    return useContext<AppContextProps>(ApplicationContext);
}