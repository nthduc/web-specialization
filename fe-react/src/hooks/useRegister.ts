import { useContext } from 'react';

import { RegisterContext, RegisterContextType } from '@/context';
function useRegister() {
    return useContext<RegisterContextType>(RegisterContext);
}
export default useRegister;
