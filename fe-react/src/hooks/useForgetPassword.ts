import { useContext } from "react";
import { ForgetPassContext, ForgetPassContextValue } from "@/context";

function useForgetPassword(){
    return useContext<ForgetPassContextValue>(ForgetPassContext);
}
export default useForgetPassword