import { useApplication } from '@/hooks';
import React, { useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';

interface ContactForm {
    value: string;
    error: string;
}

const Contact = () => {
    const location = useLocation();
    const useApp = useApplication();
    const alter = withReactContent(Swal);
    const navigate = useNavigate();

    const [title, setTitle] = useState<ContactForm>({ value: '', error: '' });
    const [content, setContent] = useState<ContactForm>({ value: '', error: '' });

    const handleChangeTitle = (event: React.ChangeEvent<HTMLInputElement>) => {
        setTitle({ ...title, value: event.target.value });
    };

    const handleChangeContent = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
        setContent({ ...content, value: event.target.value });
    };

    const handleFocusTitle = () => {
        setTitle({ ...title, error: '' });
    };

    const handleFocusContent = () => {
        setContent({ ...content, error: '' });
    };

    const handleBlurTitle = () => {
        validateTitle();
    };

    const handleBlurContent = () => {
        validateContent();
    };

    const validateTitle = () => {
        if (title.value.trim().length === 0) setTitle({ ...title, error: 'is-invalid' });
        else setTitle({ ...title, error: 'is-valid' });
    };

    const validateContent = () => {
        if (content.value.trim().length === 0) setContent({ ...content, error: 'is-invalid' });
        else setContent({ ...content, error: 'is-valid' });
    };

    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        checkValidation();
        if (title.error === 'is-valid' && content.error === 'is-valid') {
            const form = new FormData();
            const id = JSON.parse(sessionStorage.getItem('user') || '').idUser;
            form.append('idCus', id);
            form.append('title', title.value);
            form.append('content', content.value);
            fetch('http://localhost:8080/saveContact', {
                method: 'POST',
                body: form,
            })
                .then((res) => {
                    if (!res.ok) throw new Error(res.status.toString());
                    return res.json();
                })
                .then((data) => {
                    if (data.message === 'oke') {
                        alter
                            .fire({
                                icon: 'success',
                                title: 'Thành công',
                                text: 'Gửi liên hệ thành công',
                                allowOutsideClick: false,
                                showConfirmButton: true,
                                confirmButtonText: 'OK',
                            })
                            .then((result) => {
                                if (result.isConfirmed) {
                                    navigate('/contact');
                                }
                            });
                    }
                })
                .catch((err) => {
                    alter.fire({
                        icon: 'error',
                        title: 'Cập nhật không thành công',
                    });
                });
        }
    };

    const checkValidation = () => {
        if (title.value.trim().length === 0) setTitle({ ...title, error: 'is-invalid' });
        else setTitle({ ...title, error: 'is-valid' });

        if (content.value.trim().length === 0) setContent({ ...content, error: 'is-invalid' });
        else setContent({ ...content, error: 'is-valid' });
    };

    return (
        <div style={{ minHeight: '100vh', background: '#f6f9fc', padding: '50px' }}>
            <div style={{ width: '50%', margin: 'auto' }}>
                <form
                    onSubmit={handleSubmit}
                    style={{
                        padding: '50px',
                        background: '#FFFFFF',
                        borderRadius: '8px',
                        boxShadow: ' rgb(3 0 71 / 9%) 0px 1px 3px',
                    }}
                >
                    <h1 className="display-6 text-center">Liên hệ</h1>
                    <div>
                        <div>
                            <label className="form-label">Tiêu đề</label>
                            <input
                                onFocus={handleFocusTitle}
                                onBlur={handleBlurTitle}
                                onInput={handleChangeTitle}
                                value={title.value}
                                type="text"
                                className={title.error ? `form-control ${title.error}` : 'form-control'}
                                aria-describedby="validationServer03Feedback"
                            />
                            <div className="invalid-feedback">Vui lòng nhập tiêu đề!</div>
                        </div>
                        <div className="mt-5">
                            <label className="form-label">Nhập nội dung liên hệ</label>
                            <textarea
                                onFocus={handleFocusContent}
                                onBlur={handleBlurContent}
                                onInput={handleChangeContent}
                                value={content.value}
                                rows={8}
                                className={content.error ? `form-control ${content.error}` : 'form-control'}
                                placeholder="Nội dung"
                            ></textarea>
                            <div className="invalid-feedback">Vui lòng điền nội dung cần liên hệ!</div>
                        </div>
                    </div>
                    <div className="mt-5">
                        <button
                            onClick={handleSubmit as unknown as React.MouseEventHandler<HTMLButtonElement>}
                            className="btn btn-primary"
                            type="submit"
                        >
                            Gửi liên hệ
                        </button>
                    </div>
                </form>
            </div>
        </div>
    );
};

export default Contact;
