import React from 'react';
import Error404Page from './Error404';
import './error404.css';

const PageNotFound = () => {
    return (
        <div className="container-page-not-found">
            <div className="404 error">
                <Error404Page></Error404Page>
            </div>
        </div>
    );
};

export default PageNotFound;
