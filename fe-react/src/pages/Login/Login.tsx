import { FormLogin } from '@/components/FomLogin';
import { SwiperLogin } from '@/components/SwiperLogin';
import React from 'react';
import './login.css';

const Login = () => {
    return (
        <div className="container-page-login">
            <div className="page-login">
                <SwiperLogin></SwiperLogin>
                <FormLogin></FormLogin>
            </div>
        </div>
    );
};

export default Login;
