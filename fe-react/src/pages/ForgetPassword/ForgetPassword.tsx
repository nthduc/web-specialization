import React from 'react';

import './forget-password.css';
import { SwiperForgetPassword } from '@/components/SwiperForgetPassword';
import { Outlet } from 'react-router-dom';
import { ForgetPasswordProvider } from '@/context';
const ForgetPassword = () => {
    return (
        <div className="container-page-register">
            <div className="page-register">
                <SwiperForgetPassword />

                <ForgetPasswordProvider>
                    <Outlet />
                </ForgetPasswordProvider>
            </div>
        </div>
    );
};

export default ForgetPassword;
