import { Filter } from '@/components/Filter';
import React, { useEffect, useState } from 'react';
import { useSearchParams } from 'react-router-dom';
import Shop from './Shop';
import { Loading } from '@/components/Loading';
import { Pagination } from '@/components/Pagination';

interface Item {
    key: string;
    operation: string;
    value: string;
}

interface ShopProductObject {
    itemProduct: number | string;
    page: number;
    sort: string;
    list: Item[];
}

interface PaginationObject {
    offset: number;
    pageActive: number;
    sizePage: number;
    totalPage: number;
}

interface Props {
    list: any[];
}

const ShopPage = () => {
    const [searchParams] = useSearchParams();
    const [loading, setLoading] = useState<boolean>(false);
    let search = searchParams.get('search');
    const initObject = {
        itemProduct: 8,
        page: 1,
        sort: 'DES',
        list: [
            { key: 'branch', operation: 'EQUALID', value: '' },
            { key: 'color', operation: 'EQUAL', value: '' },
            { key: 'size', operation: 'EQUAL', value: '' },
            { key: 'typeShoe', operation: 'EQUAL', value: '' },
            { key: 'name', operation: 'CONTAIN', value: '' },
        ],
    };
    const pagination: PaginationObject = {
        offset: 1,
        pageActive: 1,
        sizePage: 5,
        totalPage: 0,
    };
    const [objectPagination, setPagination] = useState<PaginationObject>(pagination);
    const [listItem, setListItem] = useState<any[]>([]);
    const [shopProduct, setShopProduct] = useState<ShopProductObject>(initObject);

    const clickPage = (pageactive: number) => {
        setShopProduct({ ...shopProduct, page: pageactive });
    };

    const computeOffset = (itemPerPage: number, itemFiltered: number, pageActive: number) => {
        let totalPage = 0;
        let mod = itemFiltered % itemPerPage;
        if (mod === 0) totalPage = itemFiltered / itemPerPage;
        else totalPage = Math.floor(itemFiltered / itemPerPage) + 1;
        if (totalPage > objectPagination.sizePage) {
            if (pageActive === objectPagination.sizePage + objectPagination.offset - 1) {
                if (pageActive + objectPagination.sizePage > totalPage) {
                    return setPagination({
                        ...objectPagination,
                        pageActive: pageActive,
                        totalPage: totalPage,
                        offset: totalPage - (objectPagination.sizePage - 1),
                    });
                } else
                    return setPagination({
                        ...objectPagination,
                        pageActive: pageActive,
                        totalPage: totalPage,
                        offset: pageActive,
                    });
            }
            if (pageActive === objectPagination.offset) {
                if (pageActive - objectPagination.sizePage < 0)
                    return setPagination({
                        ...objectPagination,
                        offset: 1,
                        pageActive: pageActive,
                        totalPage: totalPage,
                    });
                else
                    return setPagination({
                        ...objectPagination,
                        offset: pageActive - objectPagination.sizePage - 1,
                        pageActive: pageActive,
                        totalPage: totalPage,
                    });
            }

            return setPagination({ ...objectPagination, pageActive: pageActive, totalPage: totalPage });
        } else setPagination({ ...objectPagination, pageActive: pageActive, totalPage: totalPage });
    };

    useEffect(() => {
        if (search) {
            let array = [...shopProduct.list];
            array[4] = { key: 'name', operation: 'CONTAIN', value: search };
            let object = { ...shopProduct, list: array };
            setShopProduct(object);
        }
    }, [search]);

    useEffect(() => {
        setLoading(true);
        fetch('http://localhost:8080/findProductByfilter', {
            method: 'POST',
            body: JSON.stringify(shopProduct),
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then((res) => {
                if (!res.ok) throw new Error(res.status.toString());
                return res.json();
            })
            .then((data) => {
                setLoading(false);
                setListItem(data.data.list);
                computeOffset(data.data.itemPerPage, data.data.itemFiltered, data.data.pageActive);
            })
            .catch((err) => {
                setLoading(false);
            });
    }, [shopProduct]);

    const changeFilter = (event: React.ChangeEvent<HTMLInputElement>, index: number) => {
        let object = { ...shopProduct.list[index], value: event.target.value };
        let arrayCoppy = [...shopProduct.list];
        arrayCoppy[index] = object;
        setShopProduct({ ...shopProduct, list: arrayCoppy, page: 1 });
    };

    const sort = (event: React.ChangeEvent<HTMLSelectElement>) => {
        setShopProduct({ ...shopProduct, sort: event.target.value });
    };

    const displayItem = (event: React.ChangeEvent<HTMLSelectElement>) => {
        setShopProduct({ ...shopProduct, itemProduct: event.target.value, page: 1 });
    };

    return (
        <div style={{ background: '#f6f9fc', minHeight: '100vh' }} className="pt-5 pb-5">
            <div className="mb-5" style={{ width: '80%', margin: 'auto' }}>
                {search && <h1 className="display-6">Kết quả tìm kiếm cho : "{search}"</h1>}
            </div>
            <Filter displayItem={displayItem} sort={sort} func={changeFilter as any}></Filter>

            {loading ? <Loading /> : <Shop list={listItem}></Shop>}

            <Pagination click={clickPage} pagination={objectPagination}></Pagination>
        </div>
    );
};

export default ShopPage;
