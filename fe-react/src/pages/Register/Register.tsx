import React from 'react';
import './register.css';
import { SwiperRegister } from '@/components/SwiperRegister';
import { Outlet } from 'react-router-dom';
import { RegisterProvider } from '@/context';

const Register = () => {
    return (
        <div className="container-page-register">
            <div className="page-register">
                <SwiperRegister />
                <RegisterProvider>
                    <Outlet />
                </RegisterProvider>
            </div>
        </div>
    );
};

export default Register;
