import React from 'react';
import { homeBanner } from '@/utils';
import { Swiper, SwiperSlide } from 'swiper/react';
import { useNavigate } from 'react-router-dom';
import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css/navigation';
import { Autoplay, Pagination } from 'swiper';
import './home.css';

const Home = () => {
    const navigate = useNavigate();
    return (
        <div className="home-page">
            <div className="home-header">
                <Swiper
                    spaceBetween={30}
                    centeredSlides={true}
                    autoplay={{
                        delay: 4000,
                        disableOnInteraction: false,
                    }}
                    pagination={{
                        clickable: true,
                    }}
                    modules={[Autoplay, Pagination]}
                >
                    <SwiperSlide>
                        <div style={{ height: '400px', width: '100%', textAlign: 'center' }}>
                            <div className="d-flex" style={{ height: '100%', width: '100%' }}>
                                <div className="bannar-text">
                                    <div className="mt-5">
                                        <h1 className="display-4 text-center">Cửa hàng giảm</h1>
                                        <h1 className="display-4"> giá 50%</h1>
                                        <button
                                            onClick={() => navigate('/shop')}
                                            type="button"
                                            className="btn btn-danger mt-5"
                                            style={{ height: '50px', width: '150px', fontSize: '20px' }}
                                        >
                                            Cửa hàng
                                        </button>
                                    </div>
                                </div>
                                <div className="bannar-img">
                                    <img src={homeBanner.imgHome2}></img>
                                </div>
                            </div>
                        </div>
                    </SwiperSlide>
                    <SwiperSlide>
                        <div style={{ height: '400px', width: '100%', textAlign: 'center' }}>
                            <div className="d-flex" style={{ height: '100%', width: '100%' }}>
                                <div className="bannar-text">
                                    <div className="mt-5">
                                        <h1 className="display-4 text-center">Cửa hàng giảm</h1>
                                        <h1 className="display-4"> giá 50%</h1>
                                        <button
                                            onClick={() => navigate('/shop')}
                                            type="button"
                                            className="btn btn-danger mt-5"
                                            style={{ height: '50px', width: '150px', fontSize: '20px' }}
                                        >
                                            Cửa hàng
                                        </button>
                                    </div>
                                </div>
                                <div className="bannar-img">
                                    <img src={homeBanner.imgHome1}></img>
                                </div>
                            </div>
                        </div>
                    </SwiperSlide>
                    <SwiperSlide>
                        <div style={{ height: '400px', width: '100%', textAlign: 'center' }}>
                            <div className="d-flex" style={{ height: '100%', width: '100%' }}>
                                <div className="bannar-text">
                                    <div className="mt-5">
                                        <h1 className="display-4 text-center">Cửa hàng giảm</h1>
                                        <h1 className="display-4"> giá 50%</h1>
                                        <button
                                            onClick={() => navigate('/shop')}
                                            type="button"
                                            className="btn btn-danger mt-5"
                                            style={{ height: '50px', width: '150px', fontSize: '20px' }}
                                        >
                                            Cửa hàng
                                        </button>
                                    </div>
                                </div>
                                <div className="bannar-img">
                                    <img src={homeBanner.imgHome3}></img>
                                </div>
                            </div>
                        </div>
                    </SwiperSlide>
                </Swiper>
            </div>
            <div className="home-contain" style={{}}>
                <div className="home-intro">
                    <div className="img-intro">
                        <img className="img-in" src={homeBanner.imgHome4}></img>
                    </div>
                    <div className="intro-text">
                        <h1>Introduce</h1>
                        <p style={{ width: '90%' }}>
                            Thể thao giúp chúng ta khỏe mạnh. Mang chúng ta đến với nhau. Thông qua thể thao, chúng ta
                            có sức mạnh để thay đổi cuộc sống. Cho dù đó là thông qua những câu chuyện của các vận động
                            viên đầy cảm hứng. Giúp bạn đứng dậy và di chuyển. Đồ thể thao có công nghệ mới nhất để nâng
                            cao hiệu suất của bạn. Bức phá giới hạn của bạn. Chúng tôi cung cấp một ngôi nhà cho người
                            chạy bộ, cầu thủ bóng rổ, trẻ em bóng đá, những người đam mê thể dục. Người đi bộ đường dài
                            cuối tuần thích trốn khỏi thành phố. Giáo viên yoga truyền bá các động tác. Giày thể thao
                            chất lượng của chúng tôi giúp bạn tập trung trước khi tiếng còi vang lên. Trong cuộc đua. Và
                            ở vạch đích. Chúng tôi hợp tác với những người giỏi nhất trong ngành để cùng sáng tạo. Bằng
                            cách này, chúng tôi cung cấp cho người hâm mộ các thể loại và kiểu dáng thể thao phù hợp với
                            nhu cầu thể thao của họ, đồng thời lưu ý đến tính bền vững. Chúng tôi ở đây để hỗ trợ người
                            sáng tạo. Cải thiện sản phẩm của họ.Tạo các thay đổi. Và chúng tôi nghĩ về tác động của
                            chúng tôi đối với thế giới.
                        </p>
                    </div>
                </div>
                <div className="home-coming">
                    <div className="title-coming">
                        <h1 className="title-new">Sản phẩm sắp ra mắt...</h1>
                    </div>
                    <div className="img-coming">
                        <div id="carouselExampleDark" className="carousel carousel-dark slide" data-bs-ride="carousel">
                            <div className="carousel-inner">
                                <div className="carousel-item active">
                                    <div
                                        style={{ height: '300px' }}
                                        className="d-flex justify-content-evenly align-items-center "
                                    >
                                        <div className="div-img">
                                            {' '}
                                            <img className="d-block w-100" src={homeBanner.imgHome5}></img>
                                        </div>
                                        <div className="div-img">
                                            {' '}
                                            <img className="d-block w-100" src={homeBanner.imgHome6}></img>
                                        </div>
                                        <div className="div-img">
                                            {' '}
                                            <img className="d-block w-100" src={homeBanner.imgHome7}></img>
                                        </div>
                                        <div className="div-img">
                                            {' '}
                                            <img className="d-block w-100" src={homeBanner.imgHome8}></img>
                                        </div>
                                    </div>
                                </div>
                                <div className="carousel-item">
                                    <div
                                        style={{ height: '300px' }}
                                        className="d-flex justify-content-evenly align-items-center "
                                    >
                                        <div className="div-img">
                                            {' '}
                                            <img className="d-block w-100" src={homeBanner.imgHome1}></img>
                                        </div>
                                        <div className="div-img">
                                            {' '}
                                            <img className="d-block w-100" src={homeBanner.imgHome1}></img>
                                        </div>
                                        <div className="div-img">
                                            {' '}
                                            <img className="d-block w-100" src={homeBanner.imgHome1}></img>
                                        </div>
                                        <div className="div-img">
                                            {' '}
                                            <img className="d-block w-100" src={homeBanner.imgHome1}></img>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button
                                className="carousel-control-prev"
                                type="button"
                                data-bs-target="#carouselExampleDark"
                                data-bs-slide="prev"
                            >
                                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span className="visually-hidden">Trở về </span>
                            </button>
                            <button
                                className="carousel-control-next"
                                type="button"
                                data-bs-target="#carouselExampleDark"
                                data-bs-slide="next"
                            >
                                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                                <span className="visually-hidden">Kế tiếp</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Home;
