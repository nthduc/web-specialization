import { ShoppingCart } from '@/components/ShoppingCart';
import React from 'react';
import './cart.css';

const Cart = () => {
    return (
        <div className="container-page-cart">
            <div className="page-cart">
                <ShoppingCart></ShoppingCart>
            </div>
        </div>
    );
};

export default Cart;
