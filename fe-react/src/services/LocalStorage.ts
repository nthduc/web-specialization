const LocalStorageApp = {
    setItemStorage: (key: string, value: string): void => {
        sessionStorage.setItem(key, value);
    },
    getItemStorage: (key: string): string | undefined => {
        const value = sessionStorage.getItem(key);
        return value !== null ? value : undefined;
    },
};

export default LocalStorageApp;
