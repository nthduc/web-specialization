const APIAuthentication = {
    signIn: (success: (data: any) => void, failure: () => void, object: any) => {
        fetch('http://localhost:8080/login', {
            method: 'POST',
            body: object,
        })
            .then((res) => {
                if (!res.ok) throw new Error(res.status.toString());
                return res.json();
            })
            .then((data) => {
                console.log(data.data);
                if (data.message === 'oke') success(data);
            })
            .catch((err) => {
                failure();
            });
    },
    signOut: (success: () => void, failure: () => void) => {
        fetch('http://localhost:8080/logout', {
            method: 'POST',
        })
            .then((res) => {
                if (!res.ok) throw new Error(res.status.toString());
                return res.json();
            })
            .then((data) => {
                console.log(data.data);
                if (data.message === 'oke') success();
            })
            .catch((err) => {
                failure();
            });
    },
};

export default APIAuthentication;
