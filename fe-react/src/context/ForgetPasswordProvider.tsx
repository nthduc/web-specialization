import { createContext, useState } from 'react';

interface FormForget {
    email: string;
    OTP: string;
    pass: string;
    formEmail: boolean;
}

export interface ForgetPassContextValue {
    form: FormForget;
    setForm: (obj: Partial<FormForget>) => void;
}

export const ForgetPassContext = createContext<ForgetPassContextValue>({
    form: {
        email: '',
        OTP: '',
        pass: '',
        formEmail: false,
    },
    setForm: () => {},
});

function ForgetPasswordProvider({ children }: { children: React.ReactNode }) {
    const formForget: FormForget = {
        email: '',
        OTP: '',
        pass: '',
        formEmail: false,
    };

    const [form, setFormForget] = useState<FormForget>(formForget);

    const setForm = (obj: Partial<FormForget>) => {
        setFormForget({ ...form, ...obj });
    };

    const value: ForgetPassContextValue = { form, setForm };

    return <ForgetPassContext.Provider value={value}>{children}</ForgetPassContext.Provider>;
}

export default ForgetPasswordProvider;
