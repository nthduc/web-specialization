import React, { createContext, PropsWithChildren, useState } from 'react';
import { LocalStorageApp } from '@/services';

interface ProviderProps extends PropsWithChildren<unknown> {}

export interface AppContextProps {
    user: string | undefined;
    logged: (object: string) => void;
    cart: Array<any>;
    addItem: (item: any) => void;
    addItemDetail: (item: any) => void;
    removeItem: (item: any) => void;
    subItem: (item: any) => void;
    setCartEmpty: () => void;
    setUserEmpty: () => void;
}

export const ApplicationContext = createContext<AppContextProps>({
    user: undefined,
    logged: () => {},
    cart: [],
    addItem: () => {},
    addItemDetail: () => {},
    removeItem: () => {},
    subItem: () => {},
    setCartEmpty: () => {},
    setUserEmpty: () => {},
});

function AppProvider({ children }: ProviderProps) {
    let init = LocalStorageApp?.getItemStorage('user');
    let shopCart = LocalStorageApp?.getItemStorage('cart');
    // State
    const [user, setUser] = useState<string | undefined>(init);
    const [cart, setCart] = useState<Array<any>>(shopCart === undefined ? [] : JSON.parse(shopCart));

    const logged = (object: string) => {
        setUser(object);
    };

    const setCartEmpty = () => {
        setCart([]);
    };
    const setUserEmpty = () => {
        setUser(undefined);
    };

    const addItem = (item: any) => {
        let arrayCopy = [...cart];
        let pos = 0;
        let product = arrayCopy.find((p, index) => {
            pos = index;
            return p.idProduct === item.idProduct;
        });
        if (product) {
            let object = { ...product, quantity: product.quantity + 1 };
            arrayCopy[pos] = object;
            setCart(arrayCopy);
            LocalStorageApp.setItemStorage('cart', JSON.stringify(arrayCopy));
        } else {
            let object = { ...item, quantity: 1 };
            let arrayProduct = [...cart, object];
            setCart(arrayProduct);
            LocalStorageApp.setItemStorage('cart', JSON.stringify(arrayProduct));
        }
    };
    const addItemDetail = (item: any) => {
        let arrayCopy = [...cart];
        let pos = 0;
        let product = arrayCopy.find((p, index) => {
            pos = index;
            return p.idProduct === item.idProduct;
        });
        if (product) {
            let object = { ...product, quantity: product.quantity + item.quantity };
            arrayCopy[pos] = object;
            setCart(arrayCopy);
            LocalStorageApp.setItemStorage('cart', JSON.stringify(arrayCopy));
        } else {
            let array = [...cart, item];
            setCart(array);
            LocalStorageApp.setItemStorage('cart', JSON.stringify(array));
        }
    };
    const removeItem = (item: any) => {
        let arrayCopy = [...cart];
        let arrayFilter = arrayCopy.filter((p) => p.idProduct !== item.idProduct);
        setCart(arrayFilter);
        LocalStorageApp.setItemStorage('cart', JSON.stringify(arrayFilter));
    };
    const subItem = (item: any) => {
        let arrayCopy = [...cart];
        let pos = 0;
        let product = arrayCopy.find((p, index) => {
            pos = index;
            return p.idProduct === item.idProduct;
        });
        if (product.quantity > 1) {
            let object = { ...product, quantity: product.quantity - 1 };
            arrayCopy[pos] = object;
            setCart(arrayCopy);
            LocalStorageApp.setItemStorage('cart', JSON.stringify(arrayCopy));
        }
    };
    let value = {
        user,
        cart,
        logged,
        addItem,
        removeItem,
        subItem,
        addItemDetail,
        setCartEmpty,
        setUserEmpty,
    };

    return <ApplicationContext.Provider value={value}>{children}</ApplicationContext.Provider>;
}

export default AppProvider;
