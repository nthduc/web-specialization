import { createContext, useState } from 'react';

interface Form {
    firstName: string;
    lastName: string;
    phone: string;
    gender: string;
    email: string;
    pass: string;
    formInFor: boolean;
    formEmail: boolean;
}

export interface RegisterContextType {
    formRegister: Form;
    setForm: (object: Partial<Form>) => void;
}

export const RegisterContext = createContext<RegisterContextType>({
    formRegister: {
        firstName: '',
        lastName: '',
        phone: '',
        gender: '',
        email: '',
        pass: '',
        formInFor: false,
        formEmail: false,
    },
    setForm: () => {},
});

interface RegisterProviderProps extends React.PropsWithChildren<unknown> {}

function RegisterProvider({ children }: RegisterProviderProps) {
    const form: Form = {
        firstName: '',
        lastName: '',
        phone: '',
        gender: '',
        email: '',
        pass: '',
        formInFor: false,
        formEmail: false,
    };

    const [formRegister, setFormRegister] = useState<Form>(form);

    const setForm = (object: Partial<Form>) => {
        console.log('setForm: ', { ...object });
        setFormRegister({ ...formRegister, ...object });
    };

    const value: RegisterContextType = { formRegister, setForm };

    return <RegisterContext.Provider value={value}>{children}</RegisterContext.Provider>;
}

export default RegisterProvider;
