import React from 'react';
import { Footer } from '@/components/Footer';
import { Header } from '@/components/Header';

import { Outlet } from 'react-router-dom';
import { Provider } from '@/context';

function Layout(): JSX.Element {
    return (
        <div>
            <Provider>
                <Header />
                <Outlet />
                <Footer />
            </Provider>
        </div>
    );
}
export default Layout;
