import React from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Autoplay, Pagination } from 'swiper';
import { Slide } from '@/components/Slide';
import { TEXT_SLIDE, TEXT_SLIDE1, TEXT_SLIDE2 } from '@/constant';
import { slideBanner } from '@/utils';

import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css/navigation';
const SwiperRegister = () => {
    return (
        <div className="container-swiper">
            <Swiper
                spaceBetween={30}
                centeredSlides={true}
                autoplay={{
                    delay: 2500,
                    disableOnInteraction: false,
                }}
                pagination={{
                    clickable: true,
                }}
                modules={[Autoplay, Pagination]}
            >
                <SwiperSlide>
                    {' '}
                    <Slide image={slideBanner.imgSlide1} title={TEXT_SLIDE}></Slide>
                </SwiperSlide>
                <SwiperSlide>
                    <Slide image={slideBanner.imgSlide2} title={TEXT_SLIDE1}></Slide>
                </SwiperSlide>
                <SwiperSlide>
                    <Slide image={slideBanner.imgSlide3} title={TEXT_SLIDE2}></Slide>
                </SwiperSlide>
            </Swiper>
            <button className="btn-register"> Đăng kí</button>
        </div>
    );
};

export default SwiperRegister;
