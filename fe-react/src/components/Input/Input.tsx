import React, { useState } from 'react';
import { ERROR_TEXT_REPEAT_PASSWORD, TEXT_REPEAT_PASS } from '@/constant';
import './input.css';
import { getErrorList } from '@/services';

interface InputState {
    value: string;
    messageError?: string;
    listError: ((value: string) => string | undefined)[];
    isErr: boolean;
}

interface ConfigProps {
    name: string;
    label: string;
    listError: string[];
    index: number;
    repeat?: {
        value: string;
        name: string;
    };
    type?: string;
    validationUrl?: {
        type: string;
        url: string;
    };
}

interface InputProps extends React.PropsWithChildren<unknown> {
    config: ConfigProps;
    inputRef: React.RefObject<HTMLInputElement>;
    onChangeParent?: (value: React.SetStateAction<string>) => void;
}

const Input = ({ children, config, inputRef, onChangeParent }: InputProps) => {
    const { name, label, listError, index, repeat, type, validationUrl } = config;

    // State
    const [showPassword, setShowPassword] = useState<boolean>(true);
    const [inputState, setInputState] = useState<InputState>({
        value: '',
        messageError: ' ',
        listError: getErrorList(listError),
        isErr: false,
    });

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
        
        if (repeat) {
           repeat.value === event.target.value  ? setInputState({ ...inputState, value: event.target.value, messageError: ' ' })
                : setInputState({
                      ...inputState,
                      value: event.target.value,
                      messageError: TEXT_REPEAT_PASS,
                  });
        } else setInputState({ ...inputState, value: event.target.value });

        function setValueForParent(value: string) {
            onChangeParent && onChangeParent(value);
        }

        // funcParent && setValueForParent(event.target.value);
        setValueForParent(event.target.value);
    };

    //

    const handleInputBlur = async (): Promise<void> => {
        if (checkInputValidity() && validationUrl) {
            if (validationUrl.type === 'email') {
                fetch(validationUrl.url + inputState.value)
                .then((res) => {
                    if (!res.ok) throw new Error(res.status.toString());
                    return res.json();
                })
                .then((data) => {
                    if (data.message === 'oke')
                    setInputState({ ...inputState, messageError: 'Email này đã được sử dụng !', isErr: true });
                    else setInputState({ ...inputState, messageError: '', isErr: false });
                })
                .catch((err) => {
                    setInputState({ ...inputState, messageError: '', isErr: true });
                });
            return;
        }
        }
    };
    //

    const checkInputValidity = (): boolean => {
        if (type && repeat && inputState.value.length !== 0) {
            repeat.value === inputState.value
                ? setInputState({ ...inputState, messageError: ' ' })
                : setInputState({ ...inputState, messageError: TEXT_REPEAT_PASS });
            return repeat.value === inputState.value;
        }
        if (listError.length === 0) return true;
        let check = false;
        inputState.listError.forEach((err) => {
            if (err(inputState.value) === undefined) {
                check = true;
                setInputState({ ...inputState, messageError: ' ' });
            } else {
                check = false;
                setInputState({ ...inputState, messageError: err(inputState.value), isErr: true });
                return;
            }
        });
        return check;
      };

    const focusInput = () => {
        setInputState({
            ...inputState,
            isErr: false,
            messageError: ' ',
        });
    };

    if (inputRef && inputRef.current) {
        (inputRef.current as any)[index] = checkInputValidity;
    }
    if (type) {
        return (
            <div className={inputState.isErr ? 'field-form text-err' : 'field-form'}>
                <input
                    onFocus={focusInput}
                    onBlur={handleInputBlur}
                    onChange={handleInputChange}
                    className={!inputState.isErr ? 'input-form' : 'input-form border-err'}
                    placeholder=" "
                    type={showPassword ? 'password' : 'text'}
                    name={name}
                    id={name}
                    value={inputState.value}
                />
                <label className={inputState.isErr ? 'label-form label-err' : 'label-form'} htmlFor={name}>
                    {label}
                </label>
                {children}
                <span className="message-error">{inputState.messageError}</span>

                {showPassword ? (
                    <div onClick={() => setShowPassword(!showPassword)} className="eye">
                        <i className="fa-solid fa-eye-slash"></i>
                    </div>
                ) : (
                    <div onClick={() => setShowPassword(!showPassword)} className="eye">
                        <i className="fa-solid fa-eye"></i>
                    </div>
                )}
            </div>
        );
    }

    return (
        <div className={inputState.isErr ? 'field-form text-err' : 'field-form'}>
            <input
                onFocus={focusInput}
                onBlur={handleInputBlur}
                onChange={handleInputChange}
                className={!inputState.isErr ? 'input-form' : 'input-form border-err'}
                placeholder=" "
                type="text"
                name={name}
                id={name}
                value={inputState.value}
            />
            <label className={inputState.isErr ? 'label-form label-err' : 'label-form'} htmlFor={name}>
                {label}
            </label>
            {children}
            <span className="message-error">{inputState.messageError}</span>
        </div>
    );
};

export default Input;
