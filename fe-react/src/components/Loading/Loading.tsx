import React from 'react';
import { loadingApp } from '@/utils';

import './loading.css';
//#22ead7
const Loading = () => {
    return (
        <div className="shoe-loading">
            <div className="container-loading">
                <img src={loadingApp.loadingGif} />
            </div>
        </div>
    );
};

export default Loading;
