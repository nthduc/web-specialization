import React, { SetStateAction, useRef, useState } from 'react';
import { useNavigate, Navigate, useLocation } from 'react-router-dom';
import { Input } from '@/components/Input';
import { useRegister } from '@/hooks';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import './form-type-email.css';
import { FIELD_EMPTY, PASSWORD, REPEAT_PASS, REQUIRE_EMAIL } from '@/constant';

const FormTypeEmail = () => {
    const alter = withReactContent(Swal);
    const location = useLocation();
    const inputRefs = useRef<any>([]);
    const navigate = useNavigate();
    const register = useRegister();

    // State
    const [valueOfPass, setValue] = useState<string>('');

    const configEmail = {
        name: 'email',
        label: 'Email*',
        listError: [REQUIRE_EMAIL],
        index: 0,
        repeat: false,
        type: false,
        url: { url: 'http://localhost:8080/isExistEmail?email=', type: 'email' },
    };

    const configPass = {
        name: 'pass',
        label: 'Mật khẩu*',
        listError: [PASSWORD],
        index: 1,
        repeat: false,
        type: true,
        url: false,
    };

    const configRepeatPass = {
        //moi lan go vao day check a?
        name: 'repeat',
        label: 'Nhập lại mật khẩu*',
        listError: [FIELD_EMPTY],
        index: 2,
        repeat: { value: valueOfPass },
        type: true,
        url: false,
    };

    ////////////////////////////////

    const submitForm = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        let isFormValid = true;

        inputRefs.current.forEach((validateInput: () => boolean) => {
            isFormValid = isFormValid && validateInput();
        });
        if (isFormValid) {
            let formData = new FormData(event.currentTarget);
            let pass = formData.get('pass');
            let email = formData.get('email');
            let formEmail = true;

            register.setForm({
                pass: pass as string,
                email: email as string,
                formEmail,
            });
            let form = new FormData();
            form.append('firstName', register.formRegister.firstName as string);
            form.append('lastName', register.formRegister.lastName as string);
            form.append('phone', register.formRegister.phone as string);
            form.append('pass', pass as string);
            form.append('email', email as string);

            console.log(register.formRegister.firstName as string);

            fetch('http://localhost:8080/customer/register', {
                method: 'POST',
                body: form,
            })
                .then((res) => {
                    if (!res.ok) throw new Error(res.status.toString());

                    return res.json();
                })
                .then((data) => {
                    console.log(data);
                    if (data.message === 'oke') {
                        Swal.fire({
                            icon: 'success',
                            title: 'Đăng kí tài khoản',
                            text: 'Bạn đăng kí tài khoản không thành công',
                            allowOutsideClick: false,
                            showConfirmButton: true,
                            confirmButtonText: 'Đănh nhập',
                        }).then((result) => {
                            if (result.isConfirmed) {
                                navigate('/');
                            }
                        });
                    }
                })
                .catch((err) => {
                    alter.fire({
                        icon: 'error',
                        title: 'Đănhg kí không thành công',
                    });
                });
        }
    };

    //
    const setValueOf = (v: SetStateAction<string>) => {
        setValue(v.valueOf() as string);
    };

    if (register.formRegister.formInFor === false) {
        return <Navigate to={'/register/formInfor'} state={{ from: location }} replace />;
    }

    return (
        <div className="container-form">
            <div className="form-register-infor">
                <div className="form-header">
                    <h1>Đăng kí tài khoản</h1>
                    <div className="display-step">
                        <span className="step">
                            <i className="fa-solid fa-check"></i>
                        </span>
                        <span className="text">Thông tin cá nhân</span>
                        <span className="line"></span>
                        <span className="step-none">2</span>
                        <span className="text">Nhập email</span>
                    </div>
                </div>
                <p> (*Bắt buộc)</p>
                <form onSubmit={submitForm}>
                    <div className="form-body">
                        <Input config={configEmail as any} inputRef={inputRefs}>
                            <i className="fa-solid fa-envelope"></i>
                        </Input>
                        <Input config={configPass as any} inputRef={inputRefs} onChangeParent={setValueOf}>
                            <i className="fa-solid fa-key"></i>
                        </Input>
                        <Input config={configRepeatPass as any} inputRef={inputRefs}>
                            <i className="fa-brands fa-react"></i>
                        </Input>
                        <div className="back-next">
                            <button onClick={() => navigate(-1)} className="back">
                                Quay lại
                            </button>
                            <button type="submit" className="next">
                                Tiếp tục
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    );
};

export default FormTypeEmail;
