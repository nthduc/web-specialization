import { useApplication } from '@/hooks';
import React, { MutableRefObject, useRef, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import logo from '/logoNike.png';
import './header.css';

interface ItemProps {
    idProduct?: number;
    thumbnail?: string;
    name?: string;
}

const Header = () => {
    // State
    const navigate = useNavigate();
    const [show, setShow] = useState<boolean>(false);
    const [active, setActive] = useState<boolean>(true);
    const [spiner, setSpiner] = useState<boolean>(false);
    const [search, setSearch] = useState<string>('');
    const [list, setList] = useState<Array<ItemProps>>([]);
    const idSetTimeOut = useRef<NodeJS.Timeout | null>(null) as MutableRefObject<NodeJS.Timeout | null>;
    const user = useApplication();

    const calculateQuantity = () => {
        let count = 0;
        user.cart.forEach((p) => {
            count += p.quantity;
        });
        return count;
    };

    const inputSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSearch(event.target.value.trim());
        if (event.target.value.trim().length === 0) setShow(false);
        else {
            setShow(true);
            if (idSetTimeOut.current) {
                clearTimeout(idSetTimeOut.current);
            }

            idSetTimeOut.current = setTimeout(() => {
                setSpiner(true);
                fetch(`http://localhost:8080/searchAutoComplete?value=${event.target.value.trim()}`)
                    .then((res) => {
                        if (!res.ok) throw new Error(res.status.toString());
                        return res.json();
                    })
                    .then((data) => {
                        if (data.message === 'oke') {
                            setSpiner(false);
                            setShow(true);
                            setList(data.data);
                        }
                    })
                    .catch((err) => {
                        setSpiner(false);
                        setShow(true);
                        setList([]);
                    });
            }, 1500);
        }
    };

    const clickIconSearch = () => {
        if (search.length !== 0) {
            setShow(false);
            navigate(`/shop?search=${search}`);
        }
    };
    const clickItem = (item: any) => {
        setShow(false);
        navigate(`/detail/${item.idProduct}`);
        setSearch('');
    };

    const activeClick = () => {
        setActive(!active);
    };

    const logOut = () => {
        sessionStorage.removeItem('user');
        user.setUserEmpty();
        navigate('/');
    };

    return (
        <header className="header">
            <div onClick={activeClick} className="menu-navigation">
                <i className="fa-solid fa-bars fa-2x"></i>
            </div>
            <div className="header-logo">
                <Link className="logo" to="/">
                    <img src={logo} alt="" />
                </Link>
            </div>
            <div className={active ? 'header-navigation' : 'header-navigation active'}>
                <div className="navigation-logo">
                    <div onClick={activeClick} className="close-btn">
                        <i className="fa-solid fa-xmark fa-2x"></i>
                    </div>
                    <div className="logo-close">
                        <div>
                            <img src={logo} alt="" />
                        </div>

                        <span>18130047@st.hcmuaf.edu.vn</span>

                        <div className="bag-responsive">
                            <i className="fa-solid fa-bag-shopping icon-bag"></i>
                            <span className="item-shopping">{calculateQuantity()}</span>
                        </div>
                    </div>
                </div>
                <hr className="hr-navigation" />
                <ul style={{ marginBottom: '0px' }}>
                    <li>
                        {' '}
                        <Link to={'/home'}>Trang chủ</Link>
                    </li>
                    <li>
                        {' '}
                        <Link to={'/shop'}>Cửa hàng</Link>
                    </li>
                    <li>
                        {' '}
                        <Link to={'/contact'}>Liên hệ</Link>
                    </li>

                    <li>
                        {' '}
                        <Link to={'/pagecart'}>Giỏ hàng</Link>
                    </li>
                    <li className="setting">
                        {' '}
                        <a href="#">Cài đặt</a>
                    </li>
                    <li className="logout">
                        {' '}
                        <a href="#">Đăng xuất</a>
                    </li>
                </ul>
            </div>

            <div className="header-search">
                <div className="search">
                    <input
                        onInput={inputSearch}
                        className="input-search bg-white"
                        type="text"
                        name="search"
                        id=""
                        value={search}
                        placeholder="Tìm kiếm"
                    />
                    <i onClick={clickIconSearch} className="fa-solid fa-magnifying-glass"></i>
                    <ul
                        style={{ maxHeight: '300px', overflowX: 'auto' }}
                        className={show ? 'search-auto-complete show' : 'search-auto-complete'}
                    >
                        {spiner ? (
                            <div className="text-center">
                                <div className="spinner-border spinner-border-sm" role="status">
                                    <span className="visually-hidden">Loading...</span>
                                </div>
                            </div>
                        ) : (
                            <>
                                {list.length === 0 ? (
                                    <div className="p-2 text-center">Không tìm thấy kết quả</div>
                                ) : (
                                    <>
                                        {list.map((item) => (
                                            <>
                                                <li onClick={() => clickItem(item)} key={item.idProduct}>
                                                    <div className="tag-auto-complete">
                                                        <img src={item.thumbnail} />
                                                    </div>
                                                    <span>{item.name}</span>
                                                </li>
                                            </>
                                        ))}
                                    </>
                                )}
                            </>
                        )}
                    </ul>
                </div>
            </div>
            <div className="bag-shopping">
                <i className="fa-solid fa-bag-shopping"></i>
                <span className="item-shopping">{calculateQuantity()}</span>
            </div>

            {user.user !== undefined ? (
                <div className="header-user">
                    <i className="fa-solid fa-user"></i>
                    <span className="name-user" style={{ minWidth: '150px' }}>
                        {JSON.parse(sessionStorage.getItem('user') || '').name}
                    </span>
                    <ul style={{ paddingLeft: '0', zIndex: '99999999' }} className="option">
                        <li>
                            <Link to={'/pageprofile'}>Cài đặt</Link>
                        </li>
                        <li style={{ paddingBottom: '5px' }}>
                            <a className="logout" onClick={logOut}>
                                {' '}
                                Đăng xuất
                            </a>
                        </li>
                    </ul>
                </div>
            ) : (
                <button onClick={() => navigate('/login')} type="button" className="btn btn-primary">
                    Đăng nhập
                </button>
            )}
        </header>
    );
};

export default Header;
