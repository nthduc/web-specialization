import React, { useState } from 'react';
import { FIELD_EMPTY, REQUIRE_EMAIL } from '@/constant';
import { getErrorList } from '@/services';
import { APIAuthentication, LocalStorageApp } from '@/services';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import { useApplication } from '@/hooks';
import { Link, useNavigate } from 'react-router-dom';

interface InputEmail {
    value: string;
    listError: ((value: string) => string | undefined)[];
    messageError: string | undefined;
    isError: boolean;
}

interface InputPass {
    value: string;
    listError: ((value: string) => string | undefined)[];
    messageError: string | undefined;
    isError: boolean;
}

const FomLogin = () => {
    const navigate = useNavigate();
    const useApp = useApplication();
    // State
    const [loading, setLoading] = useState<boolean>(false);

    const [inputEmail, setInputEmail] = useState<InputEmail>({
        value: '',
        listError: getErrorList([FIELD_EMPTY, REQUIRE_EMAIL]),
        messageError: ' ',
        isError: false,
    });

    const [inputPass, setInputPass] = useState<InputPass>({
        value: '',
        listError: getErrorList([FIELD_EMPTY]),
        messageError: ' ',
        isError: false,
    });

    const focusEmail = () => {
        setInputEmail({ ...inputEmail, messageError: ' ', isError: false });
    };

    const focusPass = () => {
        setInputPass({ ...inputPass, messageError: ' ', isError: false });
    };

    const changEmail = (event: React.ChangeEvent<HTMLInputElement>) => {
        setInputEmail({ ...inputEmail, value: event.target.value });
    };

    const changePass = (event: React.ChangeEvent<HTMLInputElement>) => {
        setInputPass({ ...inputPass, value: event.target.value });
    };

    const blurEmail = () => {
        validateEmail() &&
            fetch(`http://localhost:8080/isExistEmail?email=${inputEmail.value}`)
                .then((res) => {
                    if (!res.ok) throw new Error(res.status.toString());
                    return res.json();
                })
                .then((data) => {
                    if (data.message !== 'oke')
                        setInputEmail({ ...inputEmail, messageError: 'Emai không chính xác !', isError: true });
                })
                .catch((err) => {
                    setInputEmail({ ...inputEmail, messageError: 'Email không chính xác !', isError: true });
                });
    };

    const blurPass = () => {
        validatePass();
    };

    const validatePass = () => {
        let check = false;
        inputPass.listError.forEach((func) => {
            if (func(inputPass.value) === undefined) {
                setInputPass({ ...inputPass, messageError: ' ' });
                check = true;
            } else {
                setInputPass({ ...inputPass, messageError: func(inputPass.value), isError: true });
                check = false;
                return;
            }
        });
        // return true
        return check;
    };

    const validateEmail = () => {
        let check = false;
        inputEmail.listError.forEach((func) => {
            if (func(inputEmail.value) === undefined) {
                setInputEmail({ ...inputEmail, messageError: ' ' });
                check = true;
            } else {
                setInputEmail({ ...inputEmail, messageError: func(inputEmail.value), isError: true });
                check = false;
                return;
            }
        });
        return check;
    };

    const submit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        if (validateEmail() && validatePass()) {
            let formData = new FormData(event.currentTarget);
            let email = formData.get('email') as string;
            let pass = formData.get('pass') as string;
            login();
        }
    };

    // Login
    const login = () => {
        let form = new FormData();
        form.append('email', inputEmail.value);
        form.append('pass', inputPass.value);
        setLoading(true);
        APIAuthentication.signIn(
            (data) => {
                setLoading(true);
                navigate('/home');
                console.log(data.data);
                LocalStorageApp.setItemStorage('user', JSON.stringify(data.data));
                useApp.logged(data.data);
            },
            () => {
                setLoading(false);
                setInputPass({ ...inputPass, messageError: '', isError: true });
                setInputEmail({ ...inputEmail, messageError: '', isError: true });
                const toast = withReactContent(
                    Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 3000,
                        timerProgressBar: true,
                    }),
                );

                toast.fire({
                    icon: 'error',
                    title: 'Thông tin đăng nhập không đúng !',
                });
            },
            form,
        );
    };

    return (
        <div className="container-form">
            <h2>Chào mừng bạn đến với cửa hàng</h2>
            <form onSubmit={submit}>
                <div className={inputEmail.isError ? 'field-email text-err' : 'field-email'}>
                    <input
                        onFocus={focusEmail}
                        onBlur={blurEmail}
                        onChange={changEmail}
                        className={!inputEmail.isError ? 'input-email' : ' input-email border-err'}
                        placeholder=" "
                        type="text"
                        name="email"
                        id="email"
                        value={inputEmail.value}
                    />
                    <label className={inputEmail.isError ? 'label-email text-err' : 'label-email'} htmlFor="email">
                        Email*
                    </label>
                    <i className="fa-solid fa-envelope-open"></i>
                    <span className="message-error">{inputEmail.messageError}</span>
                </div>
                <div className={inputPass.isError ? 'field-pass text-err' : 'field-pass'}>
                    <input
                        onFocus={focusPass}
                        onBlur={blurPass}
                        onChange={changePass}
                        className={!inputPass.isError ? 'input-pass' : ' input-pass border-err'}
                        type="password"
                        name="pass"
                        id="pass"
                        placeholder=" "
                        value={inputPass.value}
                    />
                    <label className={inputPass.isError ? 'label-pass text-err' : 'label-pass'} htmlFor="pass">
                        Mật khẩu*
                    </label>
                    <i className="fa-solid fa-user-secret"></i>
                    <span className="message-error">{inputPass.messageError}</span>
                </div>

                <div className="register-forgetpass">
                    <Link to={'/forgetpass/typePass'}>Quên mật khẩu </Link>
                    <Link to={'/register/formInFor'}>Bạn chưa có tài khoản?</Link>
                </div>
                <div className="btn-login">
                    <button type="submit">
                        {loading ? (
                            <div className="spinner-border spinner-border-sm" role="status">
                                <span className="visually-hidden">Loading...</span>
                            </div>
                        ) : (
                            'Đăng nhập'
                        )}
                    </button>
                </div>
            </form>
        </div>
    );
};

export default FomLogin;

/* 
SweetAlert2 và sweetalert2-react-content là thư viện JavaScript được sử dụng để hiển thị các cửa sổ pop-up xác nhận, cảnh báo, thông báo và các thông báo tùy chỉnh khác cho người dùng trên trang web hoặc ứng dụng web. Các thông báo được tạo ra bởi SweetAlert2 có kiểu dáng đẹp mắt, tùy chỉnh cao và có tính năng tương tác linh hoạt để đáp ứng nhu cầu của người dùng.

Thư viện sweetalert2-react-content là một phiên bản được tùy chỉnh của SweetAlert2 cho phép tích hợp dễ dàng với React. Thư viện này cung cấp các thành phần React để hiển thị các cửa sổ pop-up và các thông báo tương tự như SweetAlert2, giúp cho việc tích hợp và sử dụng trên ứng dụng React trở nên dễ dàng hơn.
*/
