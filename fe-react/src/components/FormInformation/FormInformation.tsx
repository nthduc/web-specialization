import React, { useRef } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { FIELD_EMPTY, REQUIRE_PHONE } from '@/constant';
import { Input } from '@/components/Input';
import { useRegister } from '@/hooks';

interface ConfigProps {
    name: string;
    label: string;
    listError: string[];
    index: number;
    repeat: any;
    type: boolean;
    url: boolean;
}

interface InputProps {
    config: ConfigProps;
    refFunc: React.RefObject<HTMLInputElement>[];
}

function FormInformation() {
    const inputRef = useRef<(() => boolean)[]>([]);
    const navigate = useNavigate();
    const register = useRegister();
    const location = useLocation();
    console.log(location);
    const configFirstName: ConfigProps = {
        name: 'firstname',
        label: 'FirstName*',
        listError: [FIELD_EMPTY],
        index: 0,
        repeat: false,
        type: false,
        url: false,
    };
    const configLastName: ConfigProps = {
        name: 'lastname',
        label: 'LastName*',
        listError: [FIELD_EMPTY],
        index: 1,
        repeat: false,
        type: false,
        url: false,
    };
    const configPhone: ConfigProps = {
        name: 'phone',
        label: 'Phone*',
        listError: [REQUIRE_PHONE],
        index: 2,
        repeat: false,
        type: false,
        url: false,
    };

    const submitForm = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        console.log(inputRef.current);
        let check = true;

        inputRef.current.forEach((func) => {
            check = func() && check;
        });

        if (check) {
            let formData = new FormData(event.currentTarget);
            let phone = formData.get('phone');
            let lastName = formData.get('lastname');
            let firstName = formData.get('firstname');
            let formInFor = true;
            register.setForm({
                phone: phone ? phone.toString() : undefined,
                lastName: lastName ? lastName.toString() : undefined,
                firstName: firstName ? firstName.toString() : undefined,
                formInFor,
            });
            console.log('FormInfo: ', register.formRegister);
            navigate('/register/formEmail');
        }
    };

    return (
        <div className="container-form">
            <div className="form-register-infor">
                <div className="form-header">
                    <h1>Đăng kí tài khoản</h1>
                    <div className="display-step">
                        <span className="step">1</span>
                        <span className="text">Thông tin cá nhân</span>
                        <span className="line"></span>
                        <span className="step-none">2</span>
                        <span className="text">Nhập email</span>
                    </div>
                </div>
                <p> (*Bắt buộc)</p>
                <form onSubmit={submitForm}>
                    <div className="form-body">
                        <Input
                            inputRef={inputRef as unknown as React.RefObject<HTMLInputElement>}
                            config={configFirstName as any}
                        >
                            <i className="fa-solid fa-file-signature"></i>
                        </Input>
                        <Input
                            inputRef={inputRef as unknown as React.RefObject<HTMLInputElement>}
                            config={configLastName as any}
                        >
                            <i className="fa-solid fa-users"></i>
                        </Input>
                        <Input
                            inputRef={inputRef as unknown as React.RefObject<HTMLInputElement>}
                            config={configPhone as any}
                        >
                            <i className="fa-solid fa-phone"></i>
                        </Input>
                    </div>
                    <div className="form-btn">
                        <button className="submit-btn" type="submit">
                            Tiếp tục
                        </button>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default FormInformation;
