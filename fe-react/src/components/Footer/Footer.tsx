import React from 'react';

import './footer.css';
import { Link } from 'react-router-dom';

const Footer = () => {
  return (
    <>
            <footer className="py-4">
                <div className="container-xxl">
                    <div className="row align-items-center">
                        <div className="col-5">
                            <div className="footer-top-data d-flex gap-30 align-items-center">
                                <img className="email-mail" src="/email-svgrepo-com.svg" alt="Email Mail" />
                                <h2 className="mb-0 text-white">Đăng ký để nhận thông báo</h2>
                            </div>
                        </div>
                        <div className="col-7">
                            {/* INPUT */}
                            <div className="input-group">
                                <input
                                    type="text"
                                    className="form-control py-1"
                                    placeholder="Nhập địa chỉ email của bạn"
                                    aria-label="Nhập địa chỉ email của bạn"
                                    aria-describedby="basic-addon2"
                                />
                                <span className="input-group-text p-2 text-white" id="basic-addon2">
                                    Đăng ký
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <footer className="py-4">
                <div className="container-xxl">
                    <div className="row">
                        <div className="col-4">
                            <h4 className="text-white mb-4">Liên hệ với chúng tôi</h4>
                            <div>
                                <address className="text-white fs-6">
                                    Địa chỉ: KP6, Phường Linh Trung,
                                    <br /> Thành phố Thủ Đức, TP.HCM
                                    <br/> Mã số thuế : 12345678
                                </address>

                                <a href="tel:+84 962351043" className="mt-3 d-block mb-1 text-white">+84 962351043</a>
                                <a href="mailto:shoeshop@gmail.com" className="mt-2 d-block mb-0 text-white">shoeshop@gmail.com</a>
                              {/* Icon */}
                              <div className="social_icons d-flex align-items-center gap-30 mt-4">
                                <a className='text-white' href="#">
                                 {/* <BsLinkedin className='fs-4'/> */}
                                </a>
                                <a className='text-white' href="#">
                                  {/* <BsFacebook className='fs-4'/> */}
                                </a>
                                <a className='text-white' href="#">
                                  {/* <BsInstagram className='fs-4'/> */}
                                </a>
                                <a className='text-white' href="#">
                                 {/* <BsYoutube className='fs-4'/> */}
                                </a>
                              </div>
                            </div>
                        </div>
                        <div className="col-3">
                            <h4 className="text-white mb-4">Thông tin</h4>
                            <div className="footer-links d-flex flex-column">
                                <Link to="/privacy-policy" className="text-white py-2 mb-1">Chính sách bảo mật</Link>
                                <Link to="/refund-policy" className="text-white py-2 mb-1">Chính sách đổi trả</Link>
                                <Link to="/shipping-policy" className="text-white py-2 mb-1">Chính sách giao hàng</Link>
                                <Link to="/term-conditions" className="text-white py-2 mb-1">Điều khoản dịch vụ</Link>
                            </div>
                        </div>
                        <div className="col-3">
                            <h4 className="text-white mb-4">Tài khoản</h4>
                            <div className="footer-links d-flex flex-column">
                                <Link className="text-white py-2 mb-1" to={'/'}>Giới thiệu</Link>
                                <Link className="text-white py-2 mb-1" to={'/'}>Faq</Link>
                                <Link className="text-white py-2 mb-1" to={'/'}>Liên hệ</Link>
                            </div>
                        </div>
                        <div className="col-2">
                            <h4 className="text-white mb-4">Liên kết nhanh</h4>
                            <div className="footer-links d-flex flex-column">
                                <Link className="text-white py-2 mb-1" to={'/'}>Addias</Link>
                                <Link className="text-white py-2 mb-1" to={'/'}>Nike</Link>
                                <Link className="text-white py-2 mb-1" to={'/'}>Puma</Link>
                                <Link className="text-white py-2 mb-1" to={'/'}>Karwa</Link>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <footer className="py-4">
                <div className="container-xxl">
                    <div className="row">
                        <div className="col-12 mb-0 text-white">
                            <p className="text-center">
                                &copy; {new Date().getFullYear()} Công ty TNHH addidas Việt Nam
                            </p>
                        </div>
                    </div>
                </div>
            </footer>
        </>
  )
}

export default Footer