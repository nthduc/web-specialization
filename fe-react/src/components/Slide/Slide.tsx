import React from 'react';

interface SlideProps {
    image?: string;
    title?: JSX.Element;
}

const Slide = ({ image, title }: SlideProps) => {
    return (
        <div className="slide">
            <div className="slide">
                <img src={image} alt="" />
                {title}
            </div>
        </div>
    );
};

export default Slide;
