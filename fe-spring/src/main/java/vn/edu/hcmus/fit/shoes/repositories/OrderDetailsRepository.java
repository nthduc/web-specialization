package vn.edu.hcmus.fit.shoes.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.edu.hcmus.fit.shoes.models.OrderDetailsId;
import vn.edu.hcmus.fit.shoes.models.OrderDetails;

@Repository
public interface OrderDetailsRepository extends JpaRepository<OrderDetails, OrderDetailsId> {
}
