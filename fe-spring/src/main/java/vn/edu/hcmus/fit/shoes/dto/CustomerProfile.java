package vn.edu.hcmus.fit.shoes.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CustomerProfile {
    private int idCus;
    private String email;
    private String firstName;
    private String lastName;
    private String phone;
    private String linkImage;

}
