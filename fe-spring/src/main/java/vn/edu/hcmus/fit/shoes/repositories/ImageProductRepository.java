package vn.edu.hcmus.fit.shoes.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.edu.hcmus.fit.shoes.models.ImageProduct;

@Repository
public interface ImageProductRepository extends JpaRepository<ImageProduct,Integer> {
}
