package vn.edu.hcmus.fit.shoes.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ShopProduct {
    private int itemProduct ;
    private int page;
    private ArrayList<FilterCriteria> list ;
    private String sort ;
}
