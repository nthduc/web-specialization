package vn.edu.hcmus.fit.shoes.models;

import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
/*
Trong JPA (Java Persistence API), @EmbeddedId là một Annotation được sử dụng để chỉ định một thuộc tính của một Entity là một đối tượng nhúng được sử dụng làm khóa chính của Entity.

Khi một đối tượng được sử dụng làm khóa chính của Entity và không được lưu trữ trong một bảng riêng biệt, chúng ta có thể sử dụng @EmbeddedId để chỉ định trường đó là một đối tượng nhúng.

Ví dụ, giả sử chúng ta có một Entity "Order" và khóa chính của nó được tạo thành từ hai trường: "orderId" và "orderLineNumber". Trong trường hợp này, chúng ta có thể tạo một đối tượng OrderId như một đối tượng nhúng và sử dụng nó như là khóa chính của Entity:
*/

@Entity
@Table(name = "detail_order")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class OrderDetails {

    @EmbeddedId
    private OrderDetailsId id ;

    @Column(name = "quantity")
    private int quantity ;

    @ManyToOne
    @MapsId("orderId")
    private Order order;

    @ManyToOne
    @MapsId("productId")
    private Product product;

}
