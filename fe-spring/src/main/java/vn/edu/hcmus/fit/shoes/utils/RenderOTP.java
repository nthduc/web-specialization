package vn.edu.hcmus.fit.shoes.utils;

import org.springframework.stereotype.Component;

import java.util.Random;
@Component
public class RenderOTP {
    private String code ;
    public String createOTP(){
        Random random = new Random();
        StringBuilder otp  = new StringBuilder();
        for (int i =0 ; i<6 ; i++){
            int number = random.nextInt(10);
            otp.append(number);
        }
        this.code = otp.toString();
        return otp.toString();
    }
    public String getCode(){
        return code ;
    }
}