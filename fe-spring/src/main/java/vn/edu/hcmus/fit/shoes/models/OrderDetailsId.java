package vn.edu.hcmus.fit.shoes.models;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class OrderDetailsId implements Serializable {
    @Column
    private int productId ;
    @Column
    private int orderId ;
}
