package vn.edu.hcmus.fit.shoes.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.edu.hcmus.fit.shoes.dto.OrderCustomer;
import vn.edu.hcmus.fit.shoes.dto.ResponseObject;
import vn.edu.hcmus.fit.shoes.services.OrderServiceImpl;

@CrossOrigin(origins = "*")
@RestController
public class OrderController {
    @Autowired
    OrderServiceImpl orderService;

    @RequestMapping(value = "/order",method = RequestMethod.POST)
    public ResponseEntity<ResponseObject> order(@RequestBody OrderCustomer orderCustomer){
        orderService.order(orderCustomer);
        return ResponseEntity.status(HttpStatus.OK)
                .body(new ResponseObject("Ok" , "")) ;
    }
}
