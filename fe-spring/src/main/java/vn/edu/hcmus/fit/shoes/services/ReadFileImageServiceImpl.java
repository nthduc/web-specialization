package vn.edu.hcmus.fit.shoes.services;

import org.springframework.stereotype.Service;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.util.StreamUtils;

@Service
public class ReadFileImageServiceImpl implements ReadFileImageService {
    private static final Path IMAGES_PATH = Paths.get("src/main/resources/static/image");
    private static final Path THUMBNAILS_PATH = Paths.get("src/main/resources/static/thumbnail");

    @Override
    public byte[] readFileContentImage(String fileName) {
        try {
            Path file = IMAGES_PATH.resolve(fileName);
            Resource resource = new UrlResource(file.toUri());
            if(resource.exists()|| resource.isReadable()){
                return StreamUtils.copyToByteArray(resource.getInputStream());
            }else{
                throw new RuntimeException("Could not read file "+fileName);
            }
        } catch (Exception e){
            throw new RuntimeException("Could not read file "+fileName,e);
        }
    }

    @Override
    public byte[] readFileContentThumbnail(String fileName) {
        try {
            Path file = THUMBNAILS_PATH.resolve(fileName);
            Resource resource = new UrlResource(file.toUri());
            if(resource.exists()|| resource.isReadable()){
                return StreamUtils.copyToByteArray(resource.getInputStream());
            }else{
                throw new RuntimeException("Could not read file "+fileName);
            }
        } catch (Exception e){
            throw new RuntimeException("Could not read file "+fileName,e);
        }
    }
}
