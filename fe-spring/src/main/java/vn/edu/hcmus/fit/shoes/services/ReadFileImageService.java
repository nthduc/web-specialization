package vn.edu.hcmus.fit.shoes.services;

import org.springframework.stereotype.Component;

@Component
public interface ReadFileImageService {
    byte[] readFileContentImage(String fileName);
    byte[] readFileContentThumbnail(String fileName);
}
