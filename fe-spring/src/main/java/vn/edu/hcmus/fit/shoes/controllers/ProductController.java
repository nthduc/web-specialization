package vn.edu.hcmus.fit.shoes.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.edu.hcmus.fit.shoes.dto.ResponseObject;
import vn.edu.hcmus.fit.shoes.dto.ShopProduct;
import vn.edu.hcmus.fit.shoes.services.ProductServiceImpl;

@CrossOrigin(origins = "*")
@RestController
public class ProductController {
    @Autowired
    ProductServiceImpl productService;

    @RequestMapping(value = "/searchAutoComplete" , method = RequestMethod.GET)
    public ResponseEntity<ResponseObject> findNameProduct(@RequestParam("value") String value){
        return ResponseEntity.status(HttpStatus.OK)
                .body(new ResponseObject("Ok" ,productService.findProductNameContaining(value) ));

    }

    @RequestMapping(value = "/findProductById" , method = RequestMethod.GET)
    public ResponseEntity<ResponseObject> findProductById(@RequestParam("id") String id){

        return ResponseEntity.status(HttpStatus.OK)
                .body(new ResponseObject("Ok" , productService.findProductById(id)));
    }

    @RequestMapping(value = "/findProductByfilter", method = RequestMethod.POST)
    public ResponseEntity<ResponseObject> findProductByFilter( @RequestBody ShopProduct shopProduct){
        return ResponseEntity.status(HttpStatus.OK)
                .body(new ResponseObject("Ok" , productService.findProduct(shopProduct))) ;
    }
}
