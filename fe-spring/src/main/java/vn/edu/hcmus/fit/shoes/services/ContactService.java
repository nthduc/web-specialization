package vn.edu.hcmus.fit.shoes.services;

import org.springframework.stereotype.Component;
import vn.edu.hcmus.fit.shoes.models.Contact;

@Component
public interface ContactService {
    Contact saveContact(int idCus, String title, String content);
}
