package vn.edu.hcmus.fit.shoes.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.edu.hcmus.fit.shoes.models.Customer;
import vn.edu.hcmus.fit.shoes.models.Order;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Integer> {
    List<Order> findAllByCustomer(Customer customer);
}
