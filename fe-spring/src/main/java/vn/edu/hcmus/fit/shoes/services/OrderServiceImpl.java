package vn.edu.hcmus.fit.shoes.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.edu.hcmus.fit.shoes.dto.OrderCustomer;
import vn.edu.hcmus.fit.shoes.models.Customer;
import vn.edu.hcmus.fit.shoes.models.Order;
import vn.edu.hcmus.fit.shoes.models.OrderDetails;
import vn.edu.hcmus.fit.shoes.models.Product;
import vn.edu.hcmus.fit.shoes.repositories.OrderDetailsRepository;
import vn.edu.hcmus.fit.shoes.repositories.OrderRepository;
import vn.edu.hcmus.fit.shoes.utils.OrderDetailsUtils;
import vn.edu.hcmus.fit.shoes.utils.OrderUtils;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    OrderUtils orderUtils;
    @Autowired
    OrderDetailsRepository orderDetailsRepository;
    @Autowired
    OrderDetailsUtils orderDetailsUtils;
    @Autowired
    CustomerServiceImpl customerService;

    @Override
    public void order(OrderCustomer orderCustomer){
        Order o = orderUtils.toEntity(orderCustomer) ;
        Order or = orderRepository.save(o) ;
        for(Product p : orderCustomer.getListOrder()){
            OrderDetails detail = orderDetailsUtils.toEntity(p,or) ;
            orderDetailsRepository.save(detail) ;
        }
    }

    @Override
    public List<Order> findOrderByCustomer(int id){
        Customer customer = customerService.findCustomer(id);
        return orderRepository.findAllByCustomer(customer);
    }


}
