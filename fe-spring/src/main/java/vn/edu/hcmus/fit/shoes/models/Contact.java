package vn.edu.hcmus.fit.shoes.models;

import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "contact")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Contact {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private  int id ;
    private String title ;
    private String content ;
    @ManyToOne
    @JoinColumn(name = "cus_id")
    private Customer cus;

    public Contact(String title, String content, Customer customer) {
        this.title = title;
        this.content = content;
        this.cus = customer;
    }
}
