package vn.edu.hcmus.fit.shoes.models;

import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name = "branch")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Branch {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idBrand;

    @Column(name = "branch_name")
    private  String nameBranch;

    @OneToMany(mappedBy = "branch")
    private List<Product> productList ;
}
