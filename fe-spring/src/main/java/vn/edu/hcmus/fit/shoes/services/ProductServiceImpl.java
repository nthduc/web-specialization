package vn.edu.hcmus.fit.shoes.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import vn.edu.hcmus.fit.shoes.dto.ResponseShopProduct;
import vn.edu.hcmus.fit.shoes.dto.ShopProduct;
import vn.edu.hcmus.fit.shoes.models.Product;
import vn.edu.hcmus.fit.shoes.repositories.ProductRepository;
import vn.edu.hcmus.fit.shoes.utils.ProductSpecification;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService{
    @Autowired
    ProductRepository productRepository;

    // Tìm kiếm sản phẩm theo tên
    @Override
    public List<Product> findProductNameContaining(String name) {
        return productRepository.findByNameContainingIgnoreCase(name);
    }

    // Tìm kiếm sản phẩm theo ID
    @Override
    public ArrayList<Object> findProductById(String id) {
        ArrayList<Object> list = new ArrayList<>();
        Product product = productRepository.findById(Integer.parseInt(id)).get();
        list.add(product);
        list.add(product.getBranch());
        list.add(product.getListImage());
        return list;
    }

    @Override
    public Page<Product> findProductByFilter(ShopProduct shopProduct) {
        Specification<Product> productSpecification = new ProductSpecification(shopProduct.getList().get(4));
        Pageable pageable = PageRequest.of(shopProduct.getPage() - 1, shopProduct.getItemProduct(), Sort.by(shopProduct.getSort().equals("DES") ? Sort.Direction.DESC : Sort.Direction.ASC, "price"));
        for (int i = 0; i < shopProduct.getList().size() - 1; i++) {
            if (!shopProduct.getList().get(i).getValue().isBlank()) {
                productSpecification = Specification.where(productSpecification).and(new ProductSpecification(shopProduct.getList().get(i)));
            }
        }
        return productRepository.findAll(productSpecification, pageable);
    }

    @Override
    public int productFiltered (ShopProduct shopProduct){
        Specification<Product> productSpecification = new ProductSpecification(shopProduct.getList().get(4)) ;
        for(int i=0 ; i<shopProduct.getList().size()-1;i++){
            if(!shopProduct.getList().get(i).getValue().isBlank()) {
                productSpecification =  Specification.where(productSpecification).and(new ProductSpecification(shopProduct.getList().get(i)));
            }
        }
        return productRepository.findAll((Sort) productSpecification).size() ;
    }

    @Override
    public ResponseShopProduct findProduct(ShopProduct shopProduct){
        return new ResponseShopProduct(shopProduct.getItemProduct(),productFiltered(shopProduct)
                ,shopProduct.getPage(),  findProductByFilter(shopProduct).getContent());
    }
}
