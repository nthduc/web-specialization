package vn.edu.hcmus.fit.shoes.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.edu.hcmus.fit.shoes.models.Contact;
import vn.edu.hcmus.fit.shoes.models.Customer;
import vn.edu.hcmus.fit.shoes.repositories.ContactRepository;

@Service
public class ContactServiceImpl implements ContactService{
    @Autowired
    ContactRepository contactRepository;
    @Autowired
    CustomerServiceImpl service;

    @Override
    public Contact saveContact(int idCus, String title, String content) {
        Customer customer = service.findCustomer(idCus);
        Contact contact = new Contact(title, content, customer);
        return contactRepository.save(contact);
    }
}