package vn.edu.hcmus.fit.shoes.services;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import vn.edu.hcmus.fit.shoes.dto.ResponseShopProduct;
import vn.edu.hcmus.fit.shoes.dto.ShopProduct;
import vn.edu.hcmus.fit.shoes.models.Product;

import java.util.ArrayList;
import java.util.List;

@Component
public interface ProductService {
    List<Product> findProductNameContaining(String name);
    ArrayList<Object> findProductById(String id);
    Page<Product> findProductByFilter(ShopProduct shopProduct);
    int productFiltered (ShopProduct shopProduct);
    ResponseShopProduct findProduct(ShopProduct shopProduct);

}
