package vn.edu.hcmus.fit.shoes.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import vn.edu.hcmus.fit.shoes.dto.OrderCustomer;
import vn.edu.hcmus.fit.shoes.models.Customer;
import vn.edu.hcmus.fit.shoes.models.Order;
import vn.edu.hcmus.fit.shoes.repositories.CustomerRepository;

import java.util.Date;

@Component
public class OrderUtils {
    @Autowired
    CustomerRepository customer ;
    public Order toEntity(OrderCustomer order){
        Order or = new Order() ;
        or.setPhone(order.getPhone());
        or.setCompany(order.getCompany());
        or.setTimestamp(new Date());
        or.setNote(order.getNote());
        or.setStatus("NEW");
        or.setPriceOrder(order.getPriceOrder());
        Customer cus = customer.findById(order.getIdCus()).orElse(null);
        or.setCustomer(cus);
        or.setAddress(order.getAddress());

        return  or ;

    }
}
