package vn.edu.hcmus.fit.shoes.services;

import org.springframework.stereotype.Component;
import vn.edu.hcmus.fit.shoes.dto.CustomerProfile;
import vn.edu.hcmus.fit.shoes.models.Customer;

import java.util.List;

@Component
public interface CustomerService {
    List<Customer> getAllCustomers();
    boolean login(String email, String password);
    boolean isExistEmail(String email);
    boolean checkTimeSend(String email);
    boolean checkCode(String code);

    Customer findByEmail(String email);
    Customer findCustomer(int idCustomer);
    Customer saveCustomer(Customer cus);

    void updatePassword(String newPassword);
    void updateTimeAndCode(Customer customer , String code);
    void updatePassProfile(int idCus, String newPassword);
    void updateCusProfile(int idCus, String email, String firstName, String lastName, String phone);

    CustomerProfile findCustomerByFilter(int id);

}
