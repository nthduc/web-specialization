package vn.edu.hcmus.fit.shoes.services;

import org.springframework.stereotype.Component;
import vn.edu.hcmus.fit.shoes.dto.OrderCustomer;
import vn.edu.hcmus.fit.shoes.models.Order;

import java.util.List;

@Component
public interface OrderService {
    void order(OrderCustomer orderCustomer);
    List<Order> findOrderByCustomer(int id);

}
