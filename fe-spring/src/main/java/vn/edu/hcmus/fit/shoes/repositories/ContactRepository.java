package vn.edu.hcmus.fit.shoes.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.edu.hcmus.fit.shoes.models.Contact;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Integer> {
}
