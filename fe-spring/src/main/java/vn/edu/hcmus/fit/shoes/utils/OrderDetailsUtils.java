package vn.edu.hcmus.fit.shoes.utils;

import org.springframework.stereotype.Component;
import vn.edu.hcmus.fit.shoes.models.OrderDetails;
import vn.edu.hcmus.fit.shoes.models.OrderDetailsId;
import vn.edu.hcmus.fit.shoes.models.Order;
import vn.edu.hcmus.fit.shoes.models.Product;
@Component
public class OrderDetailsUtils {
    public OrderDetails toEntity(Product p , Order order){
        OrderDetails orderDetails = new OrderDetails() ;
        OrderDetailsId detailOrderId = new OrderDetailsId();
        detailOrderId.setOrderId(order.getIdOrder());
        detailOrderId.setProductId(p.getIdProduct());
        orderDetails.setId(detailOrderId);
        orderDetails.setOrder(order);
        orderDetails.setProduct(p);
        orderDetails.setQuantity(p.getQuantity());
        return orderDetails;
    }
}
