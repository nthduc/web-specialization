package vn.edu.hcmus.fit.shoes.utils;

import org.springframework.data.jpa.domain.Specification;
import vn.edu.hcmus.fit.shoes.dto.FilterCriteria;
import vn.edu.hcmus.fit.shoes.models.Product;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class ProductSpecification implements Specification<Product> {
    private FilterCriteria filterCriteria ;
    public ProductSpecification(FilterCriteria filterCriteria){
        this.filterCriteria = filterCriteria ;
    }

    @Override
    public Predicate toPredicate(Root<Product> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {

        switch (filterCriteria.getOperation()){
            case "CONTAIN" :
                return criteriaBuilder.like(root.<String>get(filterCriteria.getKey()), "%"+filterCriteria.getValue()+"%");
            case "EQUAL" :
                return criteriaBuilder.equal(root.get(filterCriteria.getKey()),filterCriteria.getValue()) ;
            case "EQUALID" :
                return criteriaBuilder.equal(root.get(filterCriteria.getKey()),Integer.parseInt(filterCriteria.getValue())) ;
            default: return null ;

        }
    }

    public FilterCriteria getFilterCriteria() {
        return filterCriteria;
    }

    public void setFilterCriteria(FilterCriteria filterCriteria) {
        this.filterCriteria = filterCriteria;
    }
}


/*
* Lớp ProductSpecification được sử dụng để tạo ra các đối tượng Specification trong Spring Data JPA. Đối tượng Specification được sử dụng để tạo ra các truy vấn có điều kiện trong JPA, giúp lấy dữ liệu từ cơ sở dữ liệu theo các tiêu chí tìm kiếm khác nhau.

Trong lớp ProductSpecification, chúng ta định nghĩa phương thức toPredicate() để tạo ra các điều kiện truy vấn trong JPA dựa trên các tiêu chí tìm kiếm được cung cấp trong đối tượng FilterCriteria. FilterCriteria chứa thông tin về thuộc tính của đối tượng Product và giá trị tìm kiếm tương ứng.

Ở đây, chúng ta sử dụng CriteriaBuilder để tạo ra các điều kiện truy vấn, tùy thuộc vào phép toán tìm kiếm được sử dụng trong FilterCriteria. Ví dụ, nếu phép toán tìm kiếm là CONTAIN, chúng ta sử dụng like để tìm kiếm các giá trị chứa chuỗi tìm kiếm. Nếu phép toán tìm kiếm là EQUAL, chúng ta sử dụng equal để tìm kiếm các giá trị bằng với giá trị tìm kiếm.
* */