package vn.edu.hcmus.fit.shoes.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.edu.hcmus.fit.shoes.services.ReadFileImageServiceImpl;

@CrossOrigin(origins = "*")
@RestController
public class ReadFileImageController {
    @Autowired
    ReadFileImageServiceImpl readFileImageService;

    @RequestMapping(value = "/thumbnail/{filename:.+}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> readDetailFileThumbnail(@PathVariable String filename){

        System.out.println(filename);
        try{
            byte[] bytes = readFileImageService.readFileContentThumbnail(filename);
            return ResponseEntity
                    .ok()
                    .contentType(MediaType.IMAGE_JPEG)
                    .body(bytes);
        } catch (Exception e){
            return ResponseEntity
                    .noContent().build();
        }

    }

    @RequestMapping(value = "/image/{filename:.+}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> readDetailFileImage(@PathVariable String filename){
        try{
            byte[] bytes = readFileImageService.readFileContentImage(filename);
            return ResponseEntity
                    .ok()
                    .contentType(MediaType.IMAGE_JPEG)
                    .body(bytes);
        } catch (Exception e){
            return ResponseEntity
                    .noContent().build();
        }

    }
}
