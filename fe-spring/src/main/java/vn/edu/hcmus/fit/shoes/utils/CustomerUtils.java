package vn.edu.hcmus.fit.shoes.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import vn.edu.hcmus.fit.shoes.dto.FormRegister;
import vn.edu.hcmus.fit.shoes.models.Customer;

@Component
public class CustomerUtils {
    @Autowired
    BCryptPasswordEncoder passwordEncoder;

    public Customer toEntity(FormRegister form) {
        Customer customer = new Customer();
        customer.setEmail(form.getEmail());
        customer.setFirstName(form.getFirstName());
        customer.setPhone(form.getPhone());
        customer.setLastName(form.getLastName());
        customer.setRole("USER");
        customer.setStatus("ACTIVE");

        String pass = form.getPass();
        if (pass != null) {
            customer.setPass(passwordEncoder.encode(pass));
        }

        return customer;
    }
}
