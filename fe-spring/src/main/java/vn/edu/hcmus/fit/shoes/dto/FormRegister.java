package vn.edu.hcmus.fit.shoes.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class FormRegister implements Serializable {
    private String firstName;
    private String lastName;
    private String phone;
    private String pass;
    private String email;
}