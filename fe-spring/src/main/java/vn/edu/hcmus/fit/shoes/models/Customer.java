package vn.edu.hcmus.fit.shoes.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;

import lombok.*;

import java.util.List;

@Entity
@Table(name = "customer")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Customer {

    @Id
    @Column(name = "customer_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idCustomer;

    @Column(name = "pass_word")
    private String pass;

    @Column(name = "phone")
    private String phone;

    @Column(name = "email")
    private String email;

    @Column(name = "role")
    private String role;

    @Column(name = "status")
    private String status;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "gender")
    private String gender;

    @Column(name = "timestamp")
    private long time;

    @Column(name = "code")
    private String code;

    @Column(name = "avatar")
    private  String linkAvatar;

    @JsonIgnore
    @OneToMany(mappedBy = "customer", fetch = FetchType.EAGER)
    private List<Order> orderList;

    @JsonIgnore
    @OneToMany(mappedBy = "cus",fetch = FetchType.LAZY)
    private List<Contact> listContact;
}
