package vn.edu.hcmus.fit.shoes.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.edu.hcmus.fit.shoes.dto.ResponseObject;
import vn.edu.hcmus.fit.shoes.services.ContactServiceImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class ContactController {
    @Autowired
    ContactServiceImpl contactService;

    @RequestMapping(value = "/saveContact" , method = RequestMethod.POST)
    public ResponseEntity<ResponseObject> saveContact(@RequestParam("idCus") int idCus,
                                                      @RequestParam("title") String title,
                                                      @RequestParam("content") String content) {
        contactService.saveContact(idCus, title, content);
        return ResponseEntity.status(HttpStatus.OK)
                .body(new ResponseObject("oke", ""));
    }
}
