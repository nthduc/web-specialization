package vn.edu.hcmus.fit.shoes.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.edu.hcmus.fit.shoes.models.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {
    Customer findCustomerByEmailAndPassAndStatus(String email , String pass , String status);
//    Customer findCustomerByEmailAndPassAndStatus(String email , String status) ;
    Customer findByEmail(String email);
}
