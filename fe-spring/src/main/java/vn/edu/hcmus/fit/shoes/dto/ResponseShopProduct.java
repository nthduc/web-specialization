package vn.edu.hcmus.fit.shoes.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.edu.hcmus.fit.shoes.models.Product;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ResponseShopProduct {
    private int itemPerPage ;
    private int itemFiltered ;
    private int pageActive ;
    private List<Product> list ;
}
