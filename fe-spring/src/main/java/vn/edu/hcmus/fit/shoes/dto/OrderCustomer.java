package vn.edu.hcmus.fit.shoes.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.edu.hcmus.fit.shoes.models.Product;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class OrderCustomer {

    private int idCus ;
    private String phone ;
    private String note ;
    private  String address;
    private String name ;
    private String company ;
    private int priceOrder ;
    private List<Product> listOrder ;
}
