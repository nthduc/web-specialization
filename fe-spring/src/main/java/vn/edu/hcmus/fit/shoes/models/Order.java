package vn.edu.hcmus.fit.shoes.models;

import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "customer_order")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "order_id")
    private int idOrder;

    @Column(name = "price_order")
    private int priceOrder;

    @ManyToOne
    @JoinColumn(name ="customer_id")
    private Customer customer;

    @Column(name = "create_order")
    private Date timestamp;

    @Column(name = "status")
    private String status;

    @Column(name = "address")
    private String address;

    @Column(name = "note_order")
    private String note;

    @Column(name="company")
    private String company;

    @Column(name = "phone")
    private String phone;

    @JsonIgnore
    @OneToMany(mappedBy = "order",fetch = FetchType.EAGER)
    private List<OrderDetails> listOrderDetails;
}
