package vn.edu.hcmus.fit.shoes.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import vn.edu.hcmus.fit.shoes.dto.FormRegister;
import vn.edu.hcmus.fit.shoes.dto.ResponseObject;
import vn.edu.hcmus.fit.shoes.models.Customer;
import vn.edu.hcmus.fit.shoes.services.CustomerServiceImpl;
import vn.edu.hcmus.fit.shoes.services.OrderServiceImpl;
import vn.edu.hcmus.fit.shoes.utils.CustomerUtils;
import vn.edu.hcmus.fit.shoes.utils.RenderOTP;
import vn.edu.hcmus.fit.shoes.utils.SendEmail;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
public class CustomerController {
        @Autowired
        CustomerServiceImpl customerService;
        @Autowired
        OrderServiceImpl orderService;
        @Autowired
        CustomerUtils customerUtils;
        @Autowired
        RenderOTP renderOTP;
        @Autowired
        SendEmail sendEmail;
        @Autowired
        BCryptPasswordEncoder passwordEncoder;

        @RequestMapping(value = "/customers", method = RequestMethod.GET)
        public ResponseEntity<List<Customer>> getAllCustomers() {
                List<Customer> customers = customerService.getAllCustomers();
                return ResponseEntity.ok(customers);
        }

        // Đằng kí
        @RequestMapping(value = "customer/register", method = RequestMethod.POST)
        public ResponseEntity<ResponseObject> register(@ModelAttribute FormRegister form) {
                System.out.println(form.getEmail());
                return ResponseEntity.status(HttpStatus.OK).body(
                                new ResponseObject("ok", customerService.saveCustomer(customerUtils.toEntity(form))));
        }

        @RequestMapping(value = "/findCus", method = RequestMethod.GET)
        public ResponseEntity<ResponseObject> findCus(@RequestParam("idCus") int idCus) {
                return ResponseEntity.status(HttpStatus.OK)
                                .body(new ResponseObject("Ok", customerService.findCustomer(idCus)));
        }

        @RequestMapping(value = "customer/updateProfile", method = RequestMethod.POST)
        public ResponseEntity<ResponseObject> updateProfile(
                        @RequestParam("idCus") int idCus,
                        @RequestParam("firstName") String firstName, @RequestParam("email") String email,
                        @RequestParam("lastName") String lastName, @RequestParam("phone") String phone) {
                customerService.updateCusProfile(idCus, email, firstName, lastName, phone);
                return ResponseEntity.status(HttpStatus.OK)
                                .body(new ResponseObject("Ok", customerService.findCustomer(idCus)));
        }

        // Lọc theo Khách hàng
        @RequestMapping(value = "/findCustomerByfilter", method = RequestMethod.GET)
        public ResponseEntity<ResponseObject> findCustomerByFilter(@RequestParam("idCus") int idCus) {
                return ResponseEntity.status(HttpStatus.OK)
                                .body(new ResponseObject("Ok", customerService.findCustomerByFilter(idCus)));
        }

        @RequestMapping(value = "customer/changePass", method = RequestMethod.POST)
        public ResponseEntity<ResponseObject> changePass(
                        @RequestParam("idCus") int idCus,
                        @RequestParam("pass") String text,
                        @RequestParam("newpass") String newpass) {
                Customer cus = customerService.findCustomer(idCus);
                String encodedPass = passwordEncoder.encode(text);
                if (passwordEncoder.matches(text, cus.getPass())) {
                        String encodedNewPass = passwordEncoder.encode(newpass);
                        customerService.updatePassProfile(idCus, encodedNewPass);
                        return ResponseEntity.status(HttpStatus.OK)
                                        .body(new ResponseObject("Ok", ""));
                }
                return ResponseEntity.status(HttpStatus.OK)
                                .body(new ResponseObject("wrong", ""));
        }

        @RequestMapping(value = "/findOrder", method = RequestMethod.GET)
        public ResponseEntity<ResponseObject> findOrder(@RequestParam("id") int id) {
                return ResponseEntity.status(HttpStatus.OK)
                                .body(new ResponseObject("Ok", orderService.findOrderByCustomer(id)));
        }

        // Reset Mật Khẩu
        @RequestMapping(value = "/resetPass", method = RequestMethod.POST)
        public ResponseEntity<ResponseObject> resetPassWord(@RequestParam("email") String email,
                        @RequestParam("otp") String otp,
                        @RequestParam("pass") String pass) {

                if (!customerService.checkTimeSend(email))
                        return ResponseEntity.status(HttpStatus.OK)
                                        .body(new ResponseObject("CODE_TIME_OUT", ""));
                if (!customerService.checkCode(otp))
                        return ResponseEntity.status(HttpStatus.OK).body(
                                        new ResponseObject("INVALID_OTP", ""));
                customerService.updatePassword(pass);
                return ResponseEntity.status(HttpStatus.OK).body(
                                new ResponseObject("Ok", ""));

        }

        @RequestMapping(value = "/sendOTP", method = RequestMethod.GET)
        public ResponseEntity<ResponseObject> sendOTP(@RequestParam("email") String email) {
                System.out.println(!customerService.isExistEmail(email));
                if (customerService.isExistEmail(email)) {
                        customerService.updateTimeAndCode(customerService.findByEmail(email), renderOTP.createOTP());
                        sendEmail.sendEmail(email, renderOTP.getCode());
                        return ResponseEntity.status(HttpStatus.OK)
                                        .body(new ResponseObject("ok", ""));
                }
                return ResponseEntity.status(HttpStatus.OK)
                                .body(new ResponseObject("NOT_EMAIL", ""));
        }

        @RequestMapping(value = "resetOTP", method = RequestMethod.GET)
        public ResponseEntity<ResponseObject> resetOTP(@RequestParam("email") String email) {
                if (customerService.checkCode(email))
                        return ResponseEntity.status(HttpStatus.OK)
                                        .body(new ResponseObject("NOT_TIME_OUT", ""));
                customerService.updateTimeAndCode(customerService.findByEmail(email), renderOTP.createOTP());
                sendEmail.sendEmail(email, renderOTP.getCode());

                return ResponseEntity.status(HttpStatus.OK)
                                .body(new ResponseObject("oke", ""));
        }
}
