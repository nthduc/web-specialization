package vn.edu.hcmus.fit.shoes.models;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "image")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ImageProduct {
    @Id
    @Column(name = "image_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idImage ;

    @Column(name = "link_image")
    private String linkImage ;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product ;
}
