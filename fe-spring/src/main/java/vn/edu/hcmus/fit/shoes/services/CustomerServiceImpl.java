package vn.edu.hcmus.fit.shoes.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import vn.edu.hcmus.fit.shoes.dto.CustomerProfile;
import vn.edu.hcmus.fit.shoes.models.Customer;
import vn.edu.hcmus.fit.shoes.repositories.CustomerRepository;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    BCryptPasswordEncoder encoder;

    private Customer cus;

    // Lấy tất cả danh sách User / Khách hàng
    @Override
    public List<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }

    @Override
    public boolean login(String email, String password) {
        Customer customer = customerRepository.findCustomerByEmailAndPassAndStatus(email, encoder.encode(password), "ACTIVE");
        if (customer != null) {
            // đăng nhập thành công
            return true;
        } else {
            // đăng nhập thất bại
            return false;
        }
    }

    @Override
    public boolean isExistEmail(String email){
        try{
            if(customerRepository.findByEmail(email)==null) return  false ;
        }catch (Exception e){
            return  false ;
        }
        return true ;
    }

    @Override
    public void updatePassword(String newPassword) {
        String hashedPassword = encoder.encode(newPassword);
        this.cus.setPass(hashedPassword);
        customerRepository.save(this.cus);
    }

    @Override
    public Customer findCustomer(int idCustomer) {
        Optional<Customer> customerOptional = customerRepository.findById(idCustomer);
        Customer customer = customerOptional.orElseThrow(() -> new RuntimeException("Customer not found"));
        return customer;
    }
    @Override
    public Customer saveCustomer(Customer cus){
        return customerRepository.save(cus) ;
    }

    @Override
    public void updateTimeAndCode(Customer customer , String code){
        customer.setCode(code);
        customer.setTime(System.currentTimeMillis());
        this.customerRepository.save(customer) ;
    }

    @Override
    public Customer findByEmail(String email){
        return  customerRepository.findByEmail(email) ;
    }

    @Override
    public boolean checkTimeSend(String email){
        Customer customer = findByEmail(email) ;
        if(customer==null) return  false ;
        this.cus = customer ;
        long start = System.currentTimeMillis()/1000;
        if((start-(customer.getTime()/1000))>60) return  false ;
        else  return  true  ;
    }

    @Override
    public boolean checkCode(String code){
        if(this.cus.getCode().equals(code)) return true ;
        return  false ;
    }

    @Override
    public void updatePassProfile(int idCus, String newPassword) {
        String hashedPassword = encoder.encode(newPassword);
        Customer cus = customerRepository.findById(idCus).orElse(null);
        assert cus != null;
        cus.setPass(hashedPassword);
        customerRepository.save(cus);
    }

    @Override
    public void updateCusProfile(int idCus, String email, String firstName, String lastName, String phone){
        Customer cus = customerRepository.findById(idCus).orElse(null);
        assert cus != null;
        cus.setEmail(email);
        cus.setFirstName(firstName);
        cus.setLastName(lastName);
        cus.setPhone(phone);
        customerRepository.save(cus);
    }

    @Override
    public CustomerProfile findCustomerByFilter(int id){
        Customer customer1 = customerRepository.findById(id).orElse(null);
        assert customer1 != null;
        CustomerProfile profile = new CustomerProfile(customer1.getIdCustomer(),
                customer1.getEmail(),customer1.getFirstName(),
                customer1.getLastName(), customer1.getPhone(), customer1.getLinkAvatar()
        );
        return  profile;
    }
    public Customer getCus(){
        return this.cus ;
    }
    }
