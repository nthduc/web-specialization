package vn.edu.hcmus.fit.shoes.models;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name = "product")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Product {
    @Id
    @Column(name = "product_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idProduct;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private int price;

    @Column(name = "status")
    private String status;

    @Column(name = "priceSale")
    private int priceSale;

    @Column(name = "thumbnail")
    private String thumbnail;

    @Column(name = "color")
    private String color;

    @Column(name = "size")
    private String size;

    @Column(name = "type_shoe")
    private String typeShoe;

    @Column(name = "description")
    private String description;

    @Column(name = "quantity")
    private int quantity;

    @JoinColumn(name = "branch_id")
    @JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL)
    private Branch branch;

    @JsonIgnore
    @OneToMany(mappedBy = "product",fetch = FetchType.EAGER)
    private List<ImageProduct> listImage;

    @JsonIgnore
    @OneToMany(mappedBy = "product")
    private List<OrderDetails> listDetailProduct ;
}
